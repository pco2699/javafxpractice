import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.Random;

public class App extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        String[] name_array = {"Morita", "Takayama", "Kimura", "Mogi"};

        // VBoxの作成
        Label your_name = new Label("Your Name");
        Label my_name = new Label("Takayama!");

        VBox vBox = new VBox(20d);
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().add(your_name);
        vBox.getChildren().add(my_name);

        // HBoxの作成
        Button change_button = new Button("Change");
        change_button.setPrefSize(100, 50);

        Button roll_button = new Button("Roll");
        roll_button.setPrefSize(100, 50);

        Button check_button = new Button("Check");
        check_button.setPrefSize(100, 50);


        change_button.setOnAction(event -> {
            // ラベルに設定する
            my_name.setText("Kimura!");
        });

        roll_button.setOnAction(event -> {
            // 乱数の生成
            Random rand = new Random();
            Timeline timer = new Timeline(new KeyFrame(Duration.millis(100), e -> {
                // 乱数を0~配列の長さで設定
                int randomNumber = rand.nextInt(name_array.length);
                my_name.setText(name_array[randomNumber]);
            }));

            timer.setCycleCount(30);
            timer.play();
        });

        check_button.setOnAction(event -> {
            if(my_name.getText().equals("Mogi")){
                // 新しいウインドウを生成
                Stage newStage = new Stage();

                // モーダルウインドウに設定
                newStage.initModality(Modality.APPLICATION_MODAL);
                // オーナーを設定
                newStage.initOwner(stage);

                // 新しいウインドウ内に配置するコンテンツを生成
                HBox hbox = new HBox();
                Label label = new Label("こんにちは！");
                hbox.getChildren().add(label);

                newStage.setScene(new Scene(hbox));

                // 新しいウインドウを表示
                newStage.show();
            }
        });


        HBox hBox = new HBox(20d);
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().add(change_button);
        hBox.getChildren().add(roll_button);
        hBox.getChildren().add(check_button);



        // BorderPaneに追加
        BorderPane borderPane = new BorderPane();
        borderPane.setPadding(new Insets(30,10,10,10));
        borderPane.setTop(vBox);
        borderPane.setCenter(hBox);

        Scene scene = new Scene(borderPane, 320, 240);
        stage.setScene(scene);
        stage.show();

    }
}
